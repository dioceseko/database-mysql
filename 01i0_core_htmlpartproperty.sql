
--
-- Table structure for table `core_htmlpartproperty`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `core_htmlpartproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `key` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `value` longtext COLLATE utf8_unicode_ci,
  `html_part_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `core_htmlpartproperty_key_ae309c7d_uniq` (`key`,`html_part_id`),
  KEY `core_htmlpartproperty_html_part_id_9b6770e9_fk_core_htmlpart_id` (`html_part_id`),
  KEY `core_htmlpartproperty_3c6e0b8a` (`key`),
  CONSTRAINT `core_htmlpartproperty_html_part_id_9b6770e9_fk_core_htmlpart_id` FOREIGN KEY (`html_part_id`) REFERENCES `core_htmlpart` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `core_htmlpartproperty`
--

