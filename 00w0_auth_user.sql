-- Table structure for table `auth_user`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `first_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(254) COLLATE utf8_unicode_ci NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1,'pbkdf2_sha256$24000$wCienlqfGuHy$3D1oN5yuiO6S/2w4O3v+GNA9y2AZP2xYnL0KtbZtDYI=','2018-01-17 10:35:23',1,'andriy','Andriy','Gushuley','andriyg@icloud.com',1,1,'2010-04-13 12:44:54'),
(2,'pbkdf2_sha256$24000$Wb2Ho2aE6L1q$DyzpqGfu9pMvFNGrTCWI37GvJKFwp+MXqS0H0g0uUTQ=','2018-03-21 16:12:28',1,'exp','Iryna','Molodiy','exp@yes.net.ua',1,1,'2010-04-22 21:45:48'),
(5,'pbkdf2_sha256$24000$JcQdkoOpRfQ0$m39qq04mRxK7ijqe0uzMcBei0I7SJy6qy9X8LVLyRg4=','2018-03-17 10:16:40',1,'percovych','','','press.ugcc.ko@gmail.com',1,1,'2012-12-13 15:03:57');
