-- Table structure for table `portal_menu`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `template_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `portal_menu_4da47e07` (`name`),
  KEY `portal_menu_43d23afc` (`template_id`),
  CONSTRAINT `template_id_refs_id_2f88c98d` FOREIGN KEY (`template_id`) REFERENCES `portal_template` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portal_menu`
--

INSERT INTO `portal_menu` (`id`, `name`, `description`, `template_id`) VALUES
(1,'top_menu','top_menu',NULL);
