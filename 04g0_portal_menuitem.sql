-- Table structure for table `portal_menuitem`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_menuitem` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `menu_id` int(11) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `non_navigable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `portal_menuitem_c379dc61` (`url`),
  KEY `portal_menuitem_08fd5523` (`menu_id`),
  KEY `portal_menuitem_410d0aac` (`parent_id`),
  CONSTRAINT `portal_menuitem_menu_id_c771540b_fk_portal_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `portal_menu` (`id`),
  CONSTRAINT `portal_menuitem_parent_id_04e18b3f_fk_portal_menuitem_id` FOREIGN KEY (`parent_id`) REFERENCES `portal_menuitem` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portal_menuitem`
--

INSERT INTO `portal_menuitem` (`id`, `caption`, `url`, `menu_id`, `parent_id`, `order`, `non_navigable`) VALUES
(1,'Фотогалереї','/photo.html',1,NULL,0,0),
(2,'Офіційні документи','/official.html',1,NULL,0,0),
(3,'Послання та проповіді','/preaching.html',1,NULL,0,0);
