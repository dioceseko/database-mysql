-- Table structure for table `portal_pageproperty`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `portal_pageproperty` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `value` varchar(30) COLLATE utf8_unicode_ci DEFAULT NULL,
  `page_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `portal_pageproperty_name_6aee02b9_uniq` (`name`,`page_id`),
  KEY `portal_pageproperty_1a63c800` (`page_id`),
  CONSTRAINT `portal_pageproperty_page_id_0cc4601e_fk_portal_page_id` FOREIGN KEY (`page_id`) REFERENCES `portal_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `portal_pageproperty`
--

