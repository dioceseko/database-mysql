-- Table structure for table `django_migrations`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1,'contenttypes','0001_initial','2017-08-10 10:44:14'),
(2,'auth','0001_initial','2017-08-10 10:44:14'),
(3,'admin','0001_initial','2017-08-10 10:44:14'),
(4,'sessions','0001_initial','2017-08-10 10:44:14'),
(5,'sites','0001_initial','2017-08-10 10:44:15'),
(6,'admin','0002_logentry_remove_auto_add','2017-09-20 10:53:49'),
(7,'contenttypes','0002_remove_content_type_name','2017-09-20 10:53:49'),
(8,'auth','0002_alter_permission_name_max_length','2017-09-20 10:53:49'),
(9,'auth','0003_alter_user_email_max_length','2017-09-20 10:53:49'),
(10,'auth','0004_alter_user_username_opts','2017-09-20 10:53:49'),
(11,'auth','0005_alter_user_last_login_null','2017-09-20 10:53:49'),
(12,'auth','0006_require_contenttypes_0002','2017-09-20 10:53:49'),
(13,'auth','0007_alter_validators_add_error_messages','2017-09-20 10:53:49'),
(14,'core','0001_initial','2017-09-20 10:53:49'),
(15,'news','0001_initial','2017-09-20 10:54:03'),
(16,'news','0002_auto_20170910_2143','2017-09-20 10:54:05'),
(17,'news','0003_auto_20170911_0957','2017-09-20 10:54:06'),
(18,'portal','0001_initial','2017-09-20 10:54:06'),
(19,'portal','0002_auto_20170907_1549','2017-09-20 10:54:06'),
(20,'prayers','0001_initial','2017-09-20 10:54:06'),
(21,'sites','0002_alter_domain_unique','2017-09-20 10:54:06'),
(22,'news','0004_remove_newsitem_anounce','2017-09-20 11:10:23'),
(23,'portal','0003_auto_20170920_2128','2017-09-27 10:14:06'),
(24,'portal','0004_auto_20170921_1155','2017-09-27 10:14:06'),
(25,'firstpage','0001_initial','2017-09-27 10:14:07'),
(26,'portal','0005_auto_20170929_1133','2017-10-17 11:10:34'),
(27,'portal','0006_auto_20171015_1116','2017-10-17 11:10:34'),
(28,'firstpage','0002_auto_20171015_1116','2017-10-17 11:10:35');
