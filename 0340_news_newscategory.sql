-- Table structure for table `news_newscategory`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_newscategory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `weight` decimal(2,0) NOT NULL,
  `unique_name` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `news_count` int(11) NOT NULL,
  `news_published_count` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `caption` (`caption`),
  UNIQUE KEY `unique_name` (`unique_name`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_newscategory`
--

INSERT INTO `news_newscategory` (`id`, `caption`, `weight`, `unique_name`, `news_count`, `news_published_count`) VALUES
(1,'Фоторепортаж',0,'photo',451,450),
(2,'Регіональні новини',0,'region',2782,2776),
(3,'Культура та Мистецтво',0,'culture',26,26),
(8,'Історія',0,'history',10,10),
(10,'Анонси',0,'anons',12,12),
(13,'Відеорепортаж',0,'videoreport',9,9),
(15,'Держава та Cуспільство',0,'state',19,19),
(19,'Релігія',0,'religion',289,289),
(23,'Церква',11,'Церква',19,19),
(24,'УГКЦ ',0,'УГКЦ',55,55);
