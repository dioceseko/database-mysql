-- Table structure for table `news_newsfeed`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news_newsfeed` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `caption` varchar(80) COLLATE utf8_unicode_ci NOT NULL,
  `two_letter_code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `template_file` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `news_count` int(11) NOT NULL,
  `news_published_count` int(11) NOT NULL,
  `not_a_feed` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `caption` (`caption`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news_newsfeed`
--

INSERT INTO `news_newsfeed` (`id`, `caption`, `two_letter_code`, `template_file`, `news_count`, `news_published_count`, `not_a_feed`) VALUES
(2,'Релігійні Свята','ho','news/ho.html',364,364,1),
(17,'Єпархія УГКЦ','ep','news/ep.html',3563,3558,0),
(24,'“Християнський вісник”','np','news/np.html',349,349,0);
