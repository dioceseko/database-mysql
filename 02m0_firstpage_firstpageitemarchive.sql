
--
-- Table structure for table `firstpage_firstpageitemarchive`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `firstpage_firstpageitemarchive` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `expire_date` date DEFAULT NULL,
  `regular_priority` int(11) NOT NULL,
  `special_priority` int(11) NOT NULL,
  `special_priority_from` date DEFAULT NULL,
  `special_priority_to` date DEFAULT NULL,
  `render_mode` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `caption` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `body` longtext COLLATE utf8_unicode_ci,
  `link` varchar(250) COLLATE utf8_unicode_ci DEFAULT NULL,
  `news_item_id` int(11) DEFAULT NULL,
  `page_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `firstpage_firstpageitemarchive_status_f96cf0f1_idx` (`status`,`date`),
  KEY `firstpage_firstpageite_news_item_id_24957dec_fk_news_newsitem_id` (`news_item_id`),
  KEY `firstpage_firstpageitemarchiv_page_id_7d1a0166_fk_portal_page_id` (`page_id`),
  KEY `firstpage_firstpageitemarchive_5fc73231` (`date`),
  CONSTRAINT `firstpage_firstpageite_news_item_id_24957dec_fk_news_newsitem_id` FOREIGN KEY (`news_item_id`) REFERENCES `news_newsitem` (`id`),
  CONSTRAINT `firstpage_firstpageitemarchiv_page_id_7d1a0166_fk_portal_page_id` FOREIGN KEY (`page_id`) REFERENCES `portal_page` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `firstpage_firstpageitemarchive`
--

