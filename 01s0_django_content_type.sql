-- Table structure for table `django_content_type`
--

/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `model` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1,'auth','permission'),
(2,'auth','group'),
(3,'auth','user'),
(5,'admin','logentry'),
(6,'contenttypes','contenttype'),
(7,'sessions','session'),
(8,'sites','site'),
(9,'news','newsfeed'),
(10,'news','newscategory'),
(11,'news','newsitem'),
(12,'news','newsitemcategory'),
(13,'rss','rssfeed'),
(14,'rss','rssfeeditem'),
(15,'business','category'),
(16,'business','organisation'),
(17,'business','organisationcategory'),
(18,'business','organisationpage'),
(19,'multihost','site'),
(20,'power','dayeventsitem'),
(21,'news','imagegallery'),
(22,'currency','exchange'),
(23,'currency','x_rate'),
(24,'business','proposal'),
(25,'business','proposalitem'),
(26,'business','div'),
(27,'business','informerproposal'),
(28,'currency','money'),
(29,'sessionprofile','sessionprofile'),
(32,'portal','template'),
(33,'portal','menu'),
(34,'portal','menuitem'),
(35,'portal','page'),
(36,'teatr','performance'),
(37,'teatr','personage'),
(38,'teatr','trouper'),
(39,'teatr','trouperrole'),
(40,'teatr','troupergallery'),
(41,'teatr','performancegallery'),
(42,'teatr','place'),
(43,'teatr','timeitem'),
(44,'teatr','announceitem'),
(45,'prayers','prayer'),
(46,'gu_multihost','site'),
(47,'firstpage','firstpageitem'),
(48,'core','htmlpart'),
(49,'core','htmlpartproperty'),
(50,'portal','pageproperty'),
(51,'portal','imagegallery'),
(52,'firstpage','firstpageitemarchive');
